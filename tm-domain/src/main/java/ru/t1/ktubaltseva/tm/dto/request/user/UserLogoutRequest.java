package ru.t1.ktubaltseva.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(@Nullable final String token) {
        super(token);
    }

}
