package ru.t1.ktubaltseva.tm.component;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.listener.EntityListener;
import ru.t1.ktubaltseva.tm.service.LoggerService;

import javax.jms.*;

public final class Bootstrap {


    private static final String URL = "failover://tcp://127.0.0.1:61616";

    private static final String QUEUE = "LOGGER";

    public void start() throws JMSException {
        @NotNull final LoggerService loggerService = new LoggerService();
        @NotNull final EntityListener entityListener = new EntityListener(loggerService);

        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();

        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);

        System.out.println("** LOGGER STARTED **");
    }
}
