package ru.t1.ktubaltseva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    void clear();

    boolean existsById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    int getSize();

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    @Nullable
    M update(@NotNull M model);

}
