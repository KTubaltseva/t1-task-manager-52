package ru.t1.ktubaltseva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IDTOService<M> {

    @NotNull
    M add(
            @Nullable String userId,
            @Nullable M model
    ) throws AbstractException;

    @NotNull
    Collection<M> add(
            @Nullable String userId,
            @Nullable Collection<M> models
    ) throws AbstractException;

    void clear(@Nullable String userId) throws AbstractException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable String userId) throws AbstractException;

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Comparator<M> comparator
    ) throws AbstractException;

    @NotNull
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws AbstractException;

    int getSize(@Nullable String userId) throws AbstractException;

    @NotNull
    void remove(
            @Nullable String userId,
            @Nullable M model
    ) throws AbstractException;

    @NotNull
    void removeById(
            @Nullable String userId,
            @Nullable String id
    ) throws AbstractException;

    @NotNull
    M update(
            @Nullable String userId,
            @Nullable M model
    ) throws AbstractException;

}
