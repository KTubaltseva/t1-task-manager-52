package ru.t1.ktubaltseva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.model.AbstractUserOwnedModelWBSDTO;

public interface IUserOwnedWBSDTORepository<M extends AbstractUserOwnedModelWBSDTO> extends IUserOwnedDTORepository<M> {

    @NotNull
    M create(
            @NotNull String userId,
            @NotNull String name
    ) throws InstantiationException, IllegalAccessException;

    @NotNull
    M create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws InstantiationException, IllegalAccessException;

}
