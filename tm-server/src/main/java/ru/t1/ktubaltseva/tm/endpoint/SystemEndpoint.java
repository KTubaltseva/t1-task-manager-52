package ru.t1.ktubaltseva.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.ktubaltseva.tm.api.service.IServiceLocator;
import ru.t1.ktubaltseva.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.ktubaltseva.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.ktubaltseva.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.ktubaltseva.tm.dto.response.system.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static ru.t1.ktubaltseva.tm.api.endpoint.IEndpoint.REQUEST;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.ktubaltseva.tm.api.endpoint.ISystemEndpoint")
public class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationAboutRequest request
    ) {
        return new ApplicationAboutResponse(
                getPropertyService().getAuthorName(),
                getPropertyService().getAuthorEmail(),
                getPropertyService().getGitBranch(),
                getPropertyService().getGitCommitId(),
                getPropertyService().getGitCommitterName(),
                getPropertyService().getGitCommitterEmail(),
                getPropertyService().getGitCommitMessage(),
                getPropertyService().getGitCommitTime()
        );
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationVersionRequest request
    ) {
        return new ApplicationVersionResponse(
                getPropertyService().getApplicationVersion()
        );
    }

}
