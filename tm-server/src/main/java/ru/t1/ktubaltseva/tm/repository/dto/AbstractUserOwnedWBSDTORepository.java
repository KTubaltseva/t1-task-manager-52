package ru.t1.ktubaltseva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.dto.IUserOwnedWBSDTORepository;
import ru.t1.ktubaltseva.tm.dto.model.AbstractUserOwnedModelWBSDTO;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedWBSDTORepository<M extends AbstractUserOwnedModelWBSDTO> extends AbstractUserOwnedDTORepository<M> implements IUserOwnedWBSDTORepository<M> {

    public AbstractUserOwnedWBSDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public M create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws InstantiationException, IllegalAccessException {
        @NotNull final M model = getClazz().newInstance();
        model.setUserId(userId);
        model.setName(name);
        return add(userId, model);
    }

    @NotNull
    @Override
    public M create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws InstantiationException, IllegalAccessException {
        @NotNull final M model = getClazz().newInstance();
        model.setUserId(userId);
        model.setName(name);
        model.setDescription(description);
        return add(userId, model);
    }

}
