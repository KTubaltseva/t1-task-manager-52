package ru.t1.ktubaltseva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.dto.ISessionDTOService;
import ru.t1.ktubaltseva.tm.dto.model.SessionDTO;
import ru.t1.ktubaltseva.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;

public class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository> implements ISessionDTOService {

    public SessionDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDTORepository(entityManager);
    }

}
