<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ru.t1.ktubaltseva.tm</groupId>
    <artifactId>tm-client</artifactId>
    <version>1.52.0</version>
    <name>task-manager-client</name>

    <parent>
        <groupId>ru.t1.ktubaltseva.tm</groupId>
        <artifactId>task-manager</artifactId>
        <version>1.52.0</version>
    </parent>

    <developers>
        <developer>
            <id>ktubaltseva</id>
            <url>https://gitlab.com/KTubaltseva</url>
            <email>ktubaltseva@t1-consulting.ru</email>
            <name>Ksenia Tubaltseva</name>
        </developer>
    </developers>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>

    <dependencies>

        <dependency>
            <groupId>ru.t1.ktubaltseva.tm</groupId>
            <artifactId>tm-domain</artifactId>
            <version>1.52.0</version>
        </dependency>

        <dependency>
            <groupId>com.jcabi</groupId>
            <artifactId>jcabi-manifests</artifactId>
            <version>1.1</version>
        </dependency>

        <dependency>
            <groupId>org.reflections</groupId>
            <artifactId>reflections</artifactId>
            <version>0.9.11</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.12</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <profiles>
        <profile>
            <id>UNIT</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <test.category>ru.t1.ktubaltseva.tm.marker.UnitCategory</test.category>
            </properties>
        </profile>
        <profile>
            <id>INTEGRATION</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <properties>
                <test.category>ru.t1.ktubaltseva.tm.marker.IntegrationCategory</test.category>
            </properties>
        </profile>
    </profiles>

    <scm>
        <connection>scm:svn:http://none</connection>
        <developerConnection>scm:svn:http://none</developerConnection>
        <url>scm:svn:http://none</url>
    </scm>

    <build>
        <finalName>tm-client</finalName>

        <plugins>

            <plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <version>4.9.10</version>
                <executions>
                    <execution>
                        <id>get-the-git-info</id>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>validate-teh-git-info</id>
                        <goals>
                            <goal>validateRevision</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <generateGitPropertiesFile>true</generateGitPropertiesFile>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>3.2.0</version>
                <configuration>
                    <outputDirectory>
                        ${project.build.directory}/lib/
                    </outputDirectory>
                    <overWriteReleases>false</overWriteReleases>
                    <overWriteSnapshots>false</overWriteSnapshots>
                    <overWriteIfNewer>true</overWriteIfNewer>
                </configuration>
                <executions>
                    <execution>
                        <id>copy-dependencies</id>
                        <phase>package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.2.0</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <classpathPrefix>lib/</classpathPrefix>
                            <mainClass>ru.t1.ktubaltseva.tm.ClientApp</mainClass>
                        </manifest>
                        <manifestEntries>
                            <groupId>${project.groupId}</groupId>
                            <artifactId>${project.artifactId}</artifactId>
                            <version>${project.version}</version>

                            <buildNumber>${buildNumber}</buildNumber>
                            <timestamp>${timestamp}</timestamp>

                            <developer>Tubaltseva Ksenia</developer>
                            <email>ktubaltseva@t1-consulting.ru</email>

                            <buildHash>${git.commit.id.abbrev}</buildHash>
                            <gitBranch>${git.branch}</gitBranch>
                            <gitCommitId>${git.commit.id}</gitCommitId>
                            <gitCommitTime>${git.commit.time}</gitCommitTime>
                            <gitCommitMessage>${git.commit.message.full}</gitCommitMessage>
                            <gitCommitterName>${git.commit.user.name}</gitCommitterName>
                            <gitCommitterEmail>${git.commit.user.email}</gitCommitterEmail>
                            <gitPipelineId>${pipeline.id}</gitPipelineId>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>1.4</version>
                <executions>
                    <execution>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <doCheck>true</doCheck>
                    <doUpdate>false</doUpdate>
                    <revisionOnScmFailure>true</revisionOnScmFailure>
                    <format>${project.version}.{0, number}</format>
                    <items>
                        <item>buildNumber</item>
                        <item>timestamp</item>
                    </items>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
                <configuration>
                    <groups>${test.category}</groups>
                </configuration>
            </plugin>

        </plugins>

    </build>

</project>
