package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktubaltseva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.ktubaltseva.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.dto.request.data.*;
import ru.t1.ktubaltseva.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserRegistryRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserRemoveRequest;
import ru.t1.ktubaltseva.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.marker.IntegrationCategory;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;

import static ru.t1.ktubaltseva.tm.constant.DomainTestData.*;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private final static ILoggerService loggerService = new LoggerService();

    @NotNull
    private final static IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final static IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final static IDomainEndpoint endpoint = IDomainEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @BeforeClass
    public static void before() throws Exception {
        @NotNull final UserLoginRequest loginAdminRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse loginAdminResponse = authEndpoint.login(loginAdminRequest);
        adminToken = loginAdminResponse.getToken();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        userEndpoint.registry(request);

        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse loginUserResponse = authEndpoint.login(loginUserRequest);
        userToken = loginUserResponse.getToken();
    }

    @AfterClass
    public static void after() throws AbstractException {
        endpoint.saveDataBackup(new DataSaveBackupRequest(adminToken));
        endpoint.loadDataBackup(new DataLoadBackupRequest(adminToken));

        @NotNull final UserLogoutRequest logoutUserRequest = new UserLogoutRequest(userToken);
        authEndpoint.logout(logoutUserRequest);
        userToken = null;

        @NotNull final UserRemoveRequest removeUserRequest = new UserRemoveRequest(adminToken, USER_LOGIN);
        userEndpoint.remove(removeUserRequest);

        @NotNull final UserLogoutRequest logoutAdminRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(logoutAdminRequest);
        adminToken = null;
    }

    @Test
    public void loadDataBackup() throws AbstractException {
        endpoint.saveDataBackup(new DataSaveBackupRequest(adminToken));
        Assert.assertNotNull(endpoint.loadDataBackup(new DataLoadBackupRequest(adminToken)));
    }

    @Test
    public void loadDataBase64() throws AbstractException {
        endpoint.saveDataBase64(new DataSaveBase64Request(adminToken));
        Assert.assertNotNull(endpoint.loadDataBase64(new DataLoadBase64Request(adminToken)));
    }

    @Test
    public void loadDataBinary() throws AbstractException {
        endpoint.saveDataBinary(new DataSaveBinaryRequest(adminToken));
        Assert.assertNotNull(endpoint.loadDataBinary(new DataLoadBinaryRequest(adminToken)));
    }

    @Test
    public void loadDataJsonFasterXml() throws AbstractException {
        endpoint.saveDataJsonFasterXml(new DataSaveJsonFasterXmlRequest(adminToken));
        Assert.assertNotNull(endpoint.loadDataJsonFasterXml(new DataLoadJsonFasterXmlRequest(adminToken)));
    }

    @Test
    public void loadDataJsonJaxB() throws AbstractException {
        endpoint.saveDataJsonJaxB(new DataSaveJsonJaxBRequest(adminToken));
        Assert.assertNotNull(endpoint.loadDataJsonJaxB(new DataLoadJsonJaxBRequest(adminToken)));
    }

    @Test
    public void loadDataXmlFasterXml() throws AbstractException {
        endpoint.saveDataXmlFasterXml(new DataSaveXmlFasterXmlRequest(adminToken));
        Assert.assertNotNull(endpoint.loadDataXmlFasterXml(new DataLoadXmlFasterXmlRequest(adminToken)));
    }

    @Test
    public void loadDataXmlJaxB() throws AbstractException {
        endpoint.saveDataXmlJaxB(new DataSaveXmlJaxBRequest(adminToken));
        Assert.assertNotNull(endpoint.loadDataXmlJaxB(new DataLoadXmlJaxBRequest(adminToken)));
    }

    @Test
    public void loadDataYamlFasterXml() throws AbstractException {
        endpoint.saveDataYamlFasterXml(new DataSaveYamlFasterXmlRequest(adminToken));
        Assert.assertNotNull(endpoint.loadDataYamlFasterXml(new DataLoadYamlFasterXmlRequest(adminToken)));
    }

    @Test
    public void saveDataBackup() throws AbstractException {
        Assert.assertNotNull(endpoint.saveDataBackup(new DataSaveBackupRequest(adminToken)));
    }

    @Test
    public void saveDataBase64() throws AbstractException {
        Assert.assertNotNull(endpoint.saveDataBase64(new DataSaveBase64Request(adminToken)));
    }

    @Test
    public void saveDataBinary() throws AbstractException {
        Assert.assertNotNull(endpoint.saveDataBinary(new DataSaveBinaryRequest(adminToken)));
    }

    @Test
    public void saveDataJsonFasterXml() throws AbstractException {
        Assert.assertNotNull(endpoint.saveDataJsonFasterXml(new DataSaveJsonFasterXmlRequest(adminToken)));
    }

    @Test
    public void saveDataJsonJaxB() throws AbstractException {
        Assert.assertNotNull(endpoint.saveDataJsonJaxB(new DataSaveJsonJaxBRequest(adminToken)));
    }

    @Test
    public void saveDataXmlFasterXml() throws AbstractException {
        Assert.assertNotNull(endpoint.saveDataXmlFasterXml(new DataSaveXmlFasterXmlRequest(adminToken)));
    }

    @Test
    public void saveDataXmlJaxB() throws AbstractException {
        Assert.assertNotNull(endpoint.saveDataXmlJaxB(new DataSaveXmlJaxBRequest(adminToken)));
    }

    @Test
    public void saveDataYamlFasterXml() throws AbstractException {
        Assert.assertNotNull(endpoint.saveDataYamlFasterXml(new DataSaveYamlFasterXmlRequest(adminToken)));
    }

}
