package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadYamlFasterXmlRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class DataLoadYamlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-yaml";

    @NotNull
    private final String DESC = "Load data from yaml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOAD YAML DATA]");
        @NotNull final DataLoadYamlFasterXmlRequest request = new DataLoadYamlFasterXmlRequest(getToken());
        getDomainEndpoint().loadDataYamlFasterXml(request);
    }

}
